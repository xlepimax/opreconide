package opreconide;
import static opreconide.Tokens.*;
%%
%class Lexer
%type Tokens
L=[a-zA-Z_]+
D=[0-9]+
SPACE=[ ,\t,\r]+
%{
    public String lexeme;
%}
%%

{SPACE} { /*Nothing to do here*/ }

( "//"(.)* ) { /*Nothing to do here*/ }

( "\n" ) {return LINE;}

( "\"" ) {lexeme=yytext(); return QUOTE;}

( if ) {lexeme=yytext(); return IF;}

( "=" ) {lexeme=yytext(); return EQUAL;}

( "+" ) {lexeme=yytext(); return ADD;}

( "-" ) {lexeme=yytext(); return MINUS;}

( "*" ) {lexeme=yytext(); return TIMES;}

( "/" ) {lexeme=yytext(); return DIVIDE;}

( "&&" | "||" | "!" | "&" | "|" ) {lexeme=yytext(); return LOGIC_OP;}

( ">" | "<" | "==" | "!=" | ">=" | "<=" | "<<" | ">>" ) {lexeme = yytext(); return RELATIONAL_OP;}

( "+=" | "-="  | "*=" | "/=" | "%=" ) {lexeme = yytext(); return ATTR_OP;}

( "++" | "--" ) {lexeme = yytext(); return ADD_OP;}

( "retorna" ) {lexeme = yytext(); return RETURN;}

( "->" ) {lexeme = yytext(); return ARROW;}

( "imprimir" ) {lexeme = yytext(); return PRINT;}

(true | false)      {lexeme = yytext(); return BOOL_OP;}

( "(" ) {lexeme=yytext(); return PA;}

( ")" ) {lexeme=yytext(); return PC;}

( "{" ) {lexeme=yytext(); return LLA;}

( "}" ) {lexeme=yytext(); return LLC;}

( "clase" ) {lexeme = yytext(); return CLASS;}

( ";" ) {lexeme=yytext(); return SEMICOLON;}

{L}({L}|{D})* {lexeme=yytext(); return IDENTIFIER;}

("(-"{D}+")")|{D}+ {lexeme=yytext(); return NUMBER;}

// DEFAULT ERROR HERE
 . {return ERROR;}
