# Grupo 7. #
# Teoría de compiladores. #

## :fire: :fire: :fire: The best group ever. :fire: :fire: :fire:
### It's not that we're so smart, it's just that we stay with problems longer.

### What is this repository for? ###

* Java application to parse and analyze mathematical expressions.
* Version 1

:fire:
Please :pray:, note you have to generate `cup` and `flex` code by invoking `Main.java`.
This program file contains the logic needed to generate the code and put them in orbit. :rocket:


### Collaborators ###

Leonel Pichardo Martínez
18-0743
lpichardomartinez@gmail.com

Andry Guerrero Yberie
18-0742
andrygtec@gmail.com

Michael Cedano
19-0891
Michael.cedano88@gmail.com

Hiram Arnaud
19-0628
hiram_arnaud@hotmail.com

Luis Enmanuel Carpio
16-0631
ecarpio16@gmail.com

Ángel Beltre
16-0904
angelbciprian@gmail.com
